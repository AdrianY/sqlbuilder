package com.greatus.sqlbuilder;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.greatus.sqlbuilder.FromClause.FromClauseForSelectSQLBuilder;
import com.greatus.sqlbuilder.WhereClause.WhereClauseForSQLSelectBuilder;

@RunWith(Parameterized.class)
public class SelectBuilderWhereClauseTest {
	private String expectedSQL;
	private WhereClauseForSQLSelectBuilder actualSQLBuilder;
	
	public SelectBuilderWhereClauseTest(StringBuilder inputExpectedSQL, WhereClauseForSQLSelectBuilder inputActualSQLBuilder){
		expectedSQL = inputExpectedSQL.toString();
		actualSQLBuilder = inputActualSQLBuilder;
	}
	
	private static StringBuilder generateBaseExpectedSQLBuilder(){
		return new StringBuilder().append("SELECT *").append("\n").append("FROM sample_table");
	}
	
	private static FromClauseForSelectSQLBuilder generateBaseActualSQLBuilder(){
		SelectBuilder selectBuiler = SQLBuilderFactory.generateSelectBuilder();
		return selectBuiler
					.select().all()
					.from().table("sample_table");
	}
	
	private static final String DATE_FORMAT = "MM/dd/YYYY";
	private static final String YEAR_MONTH_FORMAT = "MM/YYYY";
	
	@Parameters
	public static Iterable<Object[]> testParams(){
		Object[][] testParams = new Object[][]{
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field = 'name'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").equalTo("name")
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field <> 'name'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notEqualTo("name")
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field LIKE '%name%'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").like("name")
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field NOT LIKE '%name%'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notLike("name")
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field IN {'Adrian','Charmaine','Mickey'}"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").in(new String[]{"Adrian","Charmaine","Mickey"})
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field NOT IN {'Adrian','Charmaine','Mickey'}"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notIn(new String[]{"Adrian","Charmaine","Mickey"})
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field = 9"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").equalTo(9)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field <> 9"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notEqualTo(9)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field > 9"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").greaterThan(9)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field >= 9"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").greaterThanOrEqual(9)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field < 9"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").lessThan(9)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field <= 9"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").lessThanOrEqual(9)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field BETWEEN 9 AND 10"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").between(9, 10)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field NOT BETWEEN 9 AND 10"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notBetween(9, 10)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field IN {9,10,11}"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").in(new Number[]{9,10,11})
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field NOT IN {9,10,11}"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notIn(new Number[]{9,10,11})
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE TO_CHAR(my_field, 'MM/DD/YYYY') = '01/09/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").isOn(LocalDate.of(2015, Month.JANUARY, 9),DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE TO_CHAR(my_field, 'MM/DD/YYYY') <> '01/09/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").isNotOn(LocalDate.of(2015, Month.JANUARY, 9),DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE TO_CHAR(my_field, 'MM/DD/YYYY') < '01/09/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").before(LocalDate.of(2015, Month.JANUARY, 9),DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE TO_CHAR(my_field, 'MM/DD/YYYY') <= '01/09/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").onOrBefore(LocalDate.of(2015, Month.JANUARY, 9),DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE TO_CHAR(my_field, 'MM/DD/YYYY') > '01/09/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").isAfter(LocalDate.of(2015, Month.JANUARY, 9),DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE TO_CHAR(my_field, 'MM/DD/YYYY') >= '01/09/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").isOnOrAfter(LocalDate.of(2015, Month.JANUARY, 9),DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE TO_CHAR(my_field, 'MM/DD/YYYY') BETWEEN '01/09/2015' AND '07/01/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").between(LocalDate.of(2015, Month.JANUARY, 9),LocalDate.of(2015, Month.JULY, 1),DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE TO_CHAR(my_field, 'MM/DD/YYYY') NOT BETWEEN '01/09/2015' AND '07/01/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notBetween(LocalDate.of(2015, Month.JANUARY, 9),LocalDate.of(2015, Month.JULY, 1),DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field IS NULL"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").isNull()
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field IS NOT NULL"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").isNotNull()
				}
		};
		return Arrays.asList(testParams);
	}
	
	@Test
	public void shouldBeEqualToExpectedSQL() throws SQLBuilderException{
		assertEquals("Result",expectedSQL,SQLBuilderExecuter.stringifySQLBuilder(actualSQLBuilder));
	}
}
