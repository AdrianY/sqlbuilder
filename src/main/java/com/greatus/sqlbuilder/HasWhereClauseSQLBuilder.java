package com.greatus.sqlbuilder;

import com.greatus.sqlbuilder.WhereClause.AbstractWhereClauseBuilder;

interface HasWhereClauseSQLBuilder<T extends AbstractWhereClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>> {
	T getWhereClause();
	
	void setWhereClause(final T whereClause);
}
