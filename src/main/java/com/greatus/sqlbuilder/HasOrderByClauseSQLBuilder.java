package com.greatus.sqlbuilder;

import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilder;

interface HasOrderByClauseSQLBuilder {
	OrderByClauseBuilder getOrderByClause();
	
	void setOrderByClause(final OrderByClauseBuilder orderByClause);
}
