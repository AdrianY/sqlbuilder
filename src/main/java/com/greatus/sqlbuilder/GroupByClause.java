package com.greatus.sqlbuilder;

import com.greatus.sqlbuilder.HavingClause.HavingClauseBuilder;
import com.greatus.sqlbuilder.HavingClause.HavingClauseBuilderFactory;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilder;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilderFactory;

public final class GroupByClause {
	
	private GroupByClause(){}
	
	private static final String GROUPBY_CLAUSE_HEADER = "GROUP BY";
	
	static final class GroupByClauseBuilderFactory {
		
		private GroupByClauseBuilderFactory(){}
		
		static GroupByClauseBuilder generateGroupByClauseBuilder(final ActualSelectBuilder parentBuilder){
			return new GroupByClauseBuilder(parentBuilder);
		}
	}
	
	public static class GroupByClauseBuilder extends AbstractSQLSelectionClauseBuilder<ActualSelectBuilder>{
		private GroupByClauseBuilder(final ActualSelectBuilder parentBuilder){
			super(GROUPBY_CLAUSE_HEADER, parentBuilder);
		}
		
		public GroupByClauseBuilder field(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(fieldName);		
			return this;
		} 
		
		public OrderByClauseBuilder orderBy(){
			parentBuilder.setOrderByClause(OrderByClauseBuilderFactory.generateOrderByClauseBuilder(parentBuilder));
			return parentBuilder.getOrderByClause();
		}
		
		public HavingClauseBuilder having(){
			parentBuilder.setHavingClause(HavingClauseBuilderFactory.generateHavingClauseBuilder(parentBuilder));
			return parentBuilder.getHavingClause();
		}
	}
}
