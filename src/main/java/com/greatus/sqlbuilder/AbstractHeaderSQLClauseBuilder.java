package com.greatus.sqlbuilder;

public abstract class AbstractHeaderSQLClauseBuilder {
	private int hierarchyNo;
	private String sqlClauseType;
	
	AbstractHeaderSQLClauseBuilder(final String sqlClause){
		sqlClauseType = sqlClause;
		hierarchyNo = 0;
	}
	
	String getSqlClauseType(){
		return sqlClauseType;
	}

	int getHierarchyNo() {
		return hierarchyNo;
	}

	void setHierarchyNo(final int hierarchyNo) {
		this.hierarchyNo = hierarchyNo;
	}	
	
}
