package com.greatus.lazysqlbuilder;

public abstract class AbstractHeadSQLClauseBuilder {
	private int hierarchyNo;
	private HeadSQLType sqlClauseType;
	
	AbstractHeadSQLClauseBuilder(final HeadSQLType sqlClause){
		sqlClauseType = sqlClause;
		hierarchyNo = 0;
	}
	
	HeadSQLType getSqlClauseType(){
		return sqlClauseType;
	}

	int getHierarchyNo() {
		return hierarchyNo;
	}

	void setHierarchyNo(final int hierarchyNo) {
		this.hierarchyNo = hierarchyNo;
	}
}
