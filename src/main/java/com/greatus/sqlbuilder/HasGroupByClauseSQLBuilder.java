package com.greatus.sqlbuilder;

import com.greatus.sqlbuilder.GroupByClause.GroupByClauseBuilder;


interface HasGroupByClauseSQLBuilder {
	GroupByClauseBuilder getGroupByClause();
	
	void setGroupByClause(final GroupByClauseBuilder groupClause);
}
