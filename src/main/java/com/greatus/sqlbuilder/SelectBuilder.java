package com.greatus.sqlbuilder;

import com.greatus.sqlbuilder.SelectClause.SelectClauseBuilder;

public class SelectBuilder extends AbstractSQLBuilderFacade<ActualSelectBuilder>{
	
	SelectBuilder(){
		super(new ActualSelectBuilder());
	}
	
	public SelectClauseBuilder select(){
		return getWrappedActualBuilderInstance().select();
	}
}
