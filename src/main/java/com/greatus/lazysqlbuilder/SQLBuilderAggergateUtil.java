package com.greatus.lazysqlbuilder;

import static com.greatus.lazysqlbuilder.SQLBuilderStrUtil.*;

public final class SQLBuilderAggergateUtil {
	private SQLBuilderAggergateUtil(){}
	
	private static final String AGGREGATE_FX_COUNT = "COUNT";
	private static final String AGGREGATE_FX_SUM = "SUM";
	private static final String AGGREGATE_FX_MIN = "MIN";
	private static final String AGGREGATE_FX_MAX = "MAX";
	private static final String AGGREGATE_FX_DISTINCT = "DISTINCT";
	
	public static String wrapAsCountAggregate(final String fieldName){
		return performActualAggregateStringBuild(AGGREGATE_FX_COUNT, fieldName);
	}
	
	public static String wrapAsSumAggregate(final String fieldName){
		return performActualAggregateStringBuild(AGGREGATE_FX_SUM, fieldName);
	}
	
	public static String wrapAsMinAggregate(final String fieldName){
		return performActualAggregateStringBuild(AGGREGATE_FX_MIN, fieldName);
	}
	
	public static String wrapAsMaxAggregate(final String fieldName){
		return performActualAggregateStringBuild(AGGREGATE_FX_MAX, fieldName);
	}
	
	public static String wrapAsCountDistinctAggregate(final String fieldName){
		return performActualAggregateStringBuild(AGGREGATE_FX_COUNT, AGGREGATE_FX_DISTINCT + SPACE + fieldName);
	}
	
	private static String performActualAggregateStringBuild(final String sqlAggregateFunction, final String fieldName){
		return new StringBuilder(sqlAggregateFunction)
		  .append(wrapWithParenthesis(fieldName)).toString();
	}
}
