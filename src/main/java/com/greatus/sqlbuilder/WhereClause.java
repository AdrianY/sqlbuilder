package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_AND;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_NONE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_OR;

import com.greatus.sqlbuilder.ConditionGroupFactory.WhereConditionGroupForSQLSelect;
import com.greatus.sqlbuilder.ConditionFieldFactory.WhereFieldForSQLSelect;
import com.greatus.sqlbuilder.GroupByClause.GroupByClauseBuilder;
import com.greatus.sqlbuilder.GroupByClause.GroupByClauseBuilderFactory;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilder;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilderFactory;

public final class WhereClause {
	private WhereClause(){}
	
	private static final String WHERE_CLAUSE_HEADER = "WHERE";
	
	static final class WhereClauseBuilderFactory {
		static WhereClauseForSQLSelectBuilder generateWhereClauseForSQLSelectBuilder(final ActualSelectBuilder parentBuilder){
			return new WhereClauseForSQLSelectBuilder(parentBuilder);
		}
		
		private WhereClauseBuilderFactory(){}
	}
	
	static abstract class AbstractWhereClauseBuilder<T extends AbstractHeaderSQLClauseBuilder> extends AbstractSQLClauseBuilder<T>{		
		
		AbstractWhereClauseBuilder(final T parentBuilder){
			super(WHERE_CLAUSE_HEADER, parentBuilder);	
		}
		

	}
	
	public static class WhereClauseForSQLSelectBuilder extends AbstractWhereClauseBuilder<ActualSelectBuilder>{
				
		private WhereClauseForSQLSelectBuilder(final ActualSelectBuilder parentBuilder){
			super(parentBuilder);
		}
		
		public WhereFieldForSQLSelect field(final String fieldName){						
			return ConditionFieldFactory.generateWhereFieldForSelectSQL(LOGICAL_OPER_NONE, fieldName, this);
		}
		
		public WhereFieldForSQLSelect andField(final String fieldName){						
			return ConditionFieldFactory.generateWhereFieldForSelectSQL(LOGICAL_OPER_AND, fieldName, this);
		}
		
		public WhereFieldForSQLSelect orField(final String fieldName){						
			return ConditionFieldFactory.generateWhereFieldForSelectSQL(LOGICAL_OPER_OR, fieldName, this);
		}
		
		public WhereConditionGroupForSQLSelect and(){
			return ConditionGroupFactory
					.generateConditionGroupForSQLSelect(LOGICAL_OPER_AND, this);
		}
		
		public WhereConditionGroupForSQLSelect or(){
			return ConditionGroupFactory
					.generateConditionGroupForSQLSelect(LOGICAL_OPER_OR, this);
		}
		
		public GroupByClauseBuilder groupBy(){
			parentBuilder.setGroupByClause(GroupByClauseBuilderFactory.generateGroupByClauseBuilder(parentBuilder));
			return parentBuilder.getGroupByClause();
		}
		
		public OrderByClauseBuilder orderBy(){
			parentBuilder.setOrderByClause(OrderByClauseBuilderFactory.generateOrderByClauseBuilder(parentBuilder));
			return parentBuilder.getOrderByClause();
		}
	}
}	
