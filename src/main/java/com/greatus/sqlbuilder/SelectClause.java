package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsCountAggregate;
import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsCountDistinctAggregate;
import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsMaxAggregate;
import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsMinAggregate;
import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsSumAggregate;

import com.greatus.sqlbuilder.FromClause.FromClauseBuilderFactory;
import com.greatus.sqlbuilder.FromClause.FromClauseForSelectSQLBuilder;

public final class SelectClause {
	private SelectClause(){}
	
	static final String SELECT_CALUSE_HEADER = "SELECT";
	private static final String SELECT_FIELD_ALL = "*";
	
	static final class SelectClauseBuilderFactory {
		private SelectClauseBuilderFactory(){}
		
		public static SelectClauseBuilder generateSelectClauseBuilder(final ActualSelectBuilder parentBuilder){
			return new SelectClauseBuilder(parentBuilder);
		}
	}
	
	public static class SelectClauseBuilder extends AbstractSQLSelectionClauseBuilder<ActualSelectBuilder>{
		
		private SelectClauseBuilder(final ActualSelectBuilder parentBuilder){
			super(SELECT_CALUSE_HEADER, parentBuilder);
		}
		
		public SelectClauseBuilder all(){
			selectionFieldsBuilder.doAppendRawStringField(SELECT_FIELD_ALL);		
			return this;
		}
		
		public SelectClauseBuilder field(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(fieldName);		
			return this;
		}
		
		public SelectClauseBuilder countField(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(wrapAsCountAggregate(fieldName));
			return this;
		}
		
		public SelectClauseBuilder sumField(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(wrapAsSumAggregate(fieldName));
			return this;
		}
		
		public SelectClauseBuilder minField(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(wrapAsMinAggregate(fieldName));
			return this;
		}
		
		public SelectClauseBuilder maxField(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(wrapAsMaxAggregate(fieldName));
			return this;
		}
		
		public SelectClauseBuilder countDistinctField(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(wrapAsCountDistinctAggregate(fieldName));
			return this;
		}
		
		public FromClauseForSelectSQLBuilder from(){
			parentBuilder.setFromClause(FromClauseBuilderFactory.generateFromClauseForSelectSQLBuilder(parentBuilder));;
			return parentBuilder.getFromClause();
		}	
	}
}
