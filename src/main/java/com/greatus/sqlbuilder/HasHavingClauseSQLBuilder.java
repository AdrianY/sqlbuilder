package com.greatus.sqlbuilder;

import com.greatus.sqlbuilder.HavingClause.HavingClauseBuilder;

interface HasHavingClauseSQLBuilder {
	HavingClauseBuilder getHavingClause();
	
	void setHavingClause(final HavingClauseBuilder havingClause);
}
