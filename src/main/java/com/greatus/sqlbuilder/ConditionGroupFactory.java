package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.CLOSE_PARENTHESIS;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_AND;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_NONE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_OR;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.OPEN_PARENTHESIS;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.SPACE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.NEW_LINE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.TAB;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.formatDate;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapAsToCharField;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapAsValue;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.greatus.sqlbuilder.WhereClause.WhereClauseForSQLSelectBuilder;

final class ConditionGroupFactory {
	private ConditionGroupFactory(){}
	
	static WhereConditionGroupForSQLSelect generateConditionGroupForSQLSelect(final String logicalOperator, final WhereClauseForSQLSelectBuilder parentWhereBuilder){
		return new WhereConditionGroupForSQLSelect(logicalOperator, parentWhereBuilder);
	}
	
	public static class WhereConditionGroupForSQLSelect extends ConditionGroup<WhereClauseForSQLSelectBuilder>{
		
		private WhereConditionGroupForSQLSelect(final String logicalOperator, final WhereClauseForSQLSelectBuilder parentWhereBuilder){
			super(logicalOperator, parentWhereBuilder);
		}
		
		@Override
		public WhereConditionGroupFieldForSQLSelect field(
				String fieldName) {
			return new WhereConditionGroupFieldForSQLSelect(LOGICAL_OPER_NONE, fieldName, this);
		}

		@Override
		public WhereConditionGroupFieldForSQLSelect andField(
				String fieldName) {
			return new WhereConditionGroupFieldForSQLSelect(LOGICAL_OPER_AND, fieldName, this);
		}

		@Override
		public WhereConditionGroupFieldForSQLSelect orField(
				String fieldName) {
			return new WhereConditionGroupFieldForSQLSelect(LOGICAL_OPER_OR, fieldName, this);
		}
	}
	
	public static class WhereConditionGroupFieldForSQLSelect extends ConditionGroupField<WhereConditionGroupForSQLSelect>{
		private WhereConditionGroupFieldForSQLSelect(final String logicalOperator, final String fieldName, final WhereConditionGroupForSQLSelect parentGroupBuilder){
			super(logicalOperator, fieldName, parentGroupBuilder);
		}
	}
	
	static abstract class ConditionGroup<P extends AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>>{
		protected P parentConditionalClauseBuilder;
		private StringBuilder conditionGroupSQLBuilder;
		
		private ConditionGroup(final String logicalOperator, final P parentWhereBuilder){
			this.parentConditionalClauseBuilder = parentWhereBuilder;
			conditionGroupSQLBuilder = new StringBuilder();
			conditionGroupSQLBuilder
				.append(NEW_LINE)
				.append(appendableTabs(parentConditionalClauseBuilder.getParentBuilder().getHierarchyNo(), TAB))
				.append(logicalOperator+SPACE+OPEN_PARENTHESIS);
		}
		
		public abstract ConditionGroupField<? extends ConditionGroup<? extends AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>>> field(final String fieldName);
		
		public abstract ConditionGroupField<? extends ConditionGroup<? extends AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>>> andField(final String fieldName);
		
		public abstract ConditionGroupField<? extends ConditionGroup<? extends AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>>> orField(final String fieldName);
		
		public P end(){
			parentConditionalClauseBuilder.getStrBuilder()			
				.append(conditionGroupSQLBuilder.toString())
				.append(NEW_LINE)
				.append(appendableTabs(parentConditionalClauseBuilder.getParentBuilder().getHierarchyNo(), TAB))
				.append(CLOSE_PARENTHESIS);
				
			return  parentConditionalClauseBuilder;
		}
		
		StringBuilder getStrBuilder(){
			return conditionGroupSQLBuilder;
		}
	}
	
	
	
	static abstract class ConditionGroupField<P extends ConditionGroup<? extends AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>>> 
		implements ImplementableStringConditionField<P>, 
				   ImplementableNumberConditionField<P>, 
				   ImplementableDateConditionField<P, LocalDate>,
				   ImplementableNullCheckConditionField<P>{
		
		private String fieldName;
		private P parentConditionalClauseBuilder;
		private String logicalOperator;
		
		private ConditionGroupField(final String logicalOperator, final String fieldName, final P parentWhereBuilder){
			this.logicalOperator = logicalOperator;
			this.fieldName = fieldName;
			this.parentConditionalClauseBuilder = parentWhereBuilder;
		}
		
		@Override
		public P equalTo(String value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendEqualsClause(logicalOperator,fieldName,wrapAsValue(value)));
		}
		
		public P notEqualTo(final String value){
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotEqualsClause(logicalOperator,fieldName,wrapAsValue(value)));
		}
		
		@Override
		public P in(String[] value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendInClause(logicalOperator,fieldName,value,false));
		}
	
		@Override
		public P notIn(String[] value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotInClause(logicalOperator,fieldName,value,false));
		}
		
		@Override
		public P like(String value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLikeClause(logicalOperator,fieldName,value));
		}
	
		@Override
		public P notLike(String value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotLikeClause(logicalOperator,fieldName,value));
		}
	
		@Override
		public P isOn(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendEqualsClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P isNotOn(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotEqualsClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P isAfter(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendGreaterThanClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P isOnOrAfter(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendGreaterThanOrEqualsClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P before(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLessThanClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P onOrBefore(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLessThanOrEqualsClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P in(LocalDate[] value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendInClause(
							logicalOperator,wrapAsToCharField(fieldName, format), 
							stringifyArray(finalizetoBeStringifiedArray(value, format)), false));
		}
	
		@Override
		public P notIn(LocalDate[] value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotInClause(
							logicalOperator,wrapAsToCharField(fieldName, format), 
							stringifyArray(finalizetoBeStringifiedArray(value, format)), false));
		}
	
		@Override
		public P between(LocalDate lowerBoundValue,
				LocalDate upperBoundValue, String format) {
			return appendToParentBuilder(
						SQLBuilderConditionFieldUtil.appendBetweenClause(
								logicalOperator,
								wrapAsToCharField(fieldName, format), 
								wrapAsValue(formatDate(lowerBoundValue, format)), 
								wrapAsValue(formatDate(upperBoundValue, format))
						)
					);
		}
	
		@Override
		public P notBetween(LocalDate lowerBoundValue,
				LocalDate upperBoundValue, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotBetweenClause(
							logicalOperator,
							wrapAsToCharField(fieldName, format), 
							wrapAsValue(formatDate(lowerBoundValue, format)), 
							wrapAsValue(formatDate(upperBoundValue, format))
					)
				);
		}
	
		@Override
		public P equalTo(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendEqualsClause(
							logicalOperator, fieldName,value.toString()));
		}
	
		@Override
		public P notEqualTo(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotEqualsClause(
							logicalOperator, fieldName,value.toString()));
		}
	
		@Override
		public P greaterThan(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendGreaterThanClause(logicalOperator,fieldName,value.toString()));
		}
	
		@Override
		public P greaterThanOrEqual(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendGreaterThanOrEqualsClause(logicalOperator,fieldName,value.toString()));
		}
	
		@Override
		public P lessThan(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLessThanClause(
							logicalOperator, fieldName,value.toString()));
		}
	
		@Override
		public P lessThanOrEqual(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLessThanOrEqualsClause(logicalOperator,fieldName,value.toString()));
		}
	
		@Override
		public P in(Number[] value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendInClause(
							logicalOperator, fieldName,stringifyArray(Arrays.asList(value)),true));
		}
	
		@Override
		public P notIn(Number[] value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotInClause(
							logicalOperator, fieldName,stringifyArray(Arrays.asList(value)), true));
		}
	
		@Override
		public P between(Number lowerBoundValue,
				Number upperBoundValue) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendBetweenClause(
							logicalOperator, fieldName,lowerBoundValue.toString(), upperBoundValue.toString()));
		}
	
		@Override
		public P notBetween(Number lowerBoundValue,
				Number upperBoundValue) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotBetweenClause(
							logicalOperator, fieldName,lowerBoundValue.toString(), upperBoundValue.toString()));
		}
		
		public P isNull(){
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendIsNullClause(logicalOperator, fieldName));
		}
		
		public P isNotNull(){
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendIsNotNullClause(logicalOperator, fieldName));
		}
		@SuppressWarnings("unchecked")
		private static <T> String[] stringifyArray(final List<T> listOfValues){
			String[] stringifiedNumberArray = {};
			
			T[] arrOfValues = (T[]) listOfValues.toArray();
			if(null != arrOfValues && 0 < arrOfValues.length){
				Arrays.sort(arrOfValues);
				stringifiedNumberArray = Arrays.toString(arrOfValues).split("[\\[\\]]")[1].split(", "); 
			}
			
			return stringifiedNumberArray;
		}
		
		private static <T>  List<String> finalizetoBeStringifiedArray(final T[] arrayToBeStringified, final String format){
			List<String> returnValue = new ArrayList<String>();
			if(null != arrayToBeStringified && LocalDate.class.equals(arrayToBeStringified.getClass().getComponentType())){
				for(T elementtoBeStringified: arrayToBeStringified){
					returnValue.add(formatDate(LocalDate.class.cast(elementtoBeStringified), format));
				}
			} 			
			
			return returnValue;
		}
					
		private P appendToParentBuilder(final String conditionClause){
			String builtString = NEW_LINE + appendableTabs(parentConditionalClauseBuilder.parentConditionalClauseBuilder.getParentBuilder().getHierarchyNo(), TAB + TAB);
			parentConditionalClauseBuilder.getStrBuilder().append(builtString).append(conditionClause);
			return parentConditionalClauseBuilder;
		}
	}
	
	private static String appendableTabs(final int requiredNoOfTabs, final String initialValue){
		String appendableTabs = initialValue;
		for(int i = 0; i < requiredNoOfTabs; i++){
			appendableTabs += TAB;
		}
		return appendableTabs;
	}
}
