package com.greatus.sqlbuilder;

public interface ImplementableNumberConditionField<T/* extends AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>*/>{

	T equalTo(final Number value);
	
	T notEqualTo(final Number value);
	
	T greaterThan(final Number value);
	
	T greaterThanOrEqual(final Number value);
	
	T lessThan(final Number value);
	
	T lessThanOrEqual(final Number value);
	
	T in(final Number[] value);
	
	T notIn(final Number[] value);
	
	T between(final Number lowerBoundValue, final Number upperBoundValue);
	
	T notBetween(final Number lowerBoundValue, final Number upperBoundValue);

}
