package com.greatus.sqlbuilder;

import com.greatus.sqlbuilder.SelectionFieldBuilderFactory.SelectionFieldBuilder;

abstract class AbstractSQLSelectionClauseBuilder<T extends AbstractHeaderSQLClauseBuilder> extends
		AbstractSQLClauseBuilder<T> {

	protected SelectionFieldBuilder selectionFieldsBuilder;
	
	AbstractSQLSelectionClauseBuilder(final String clauseHeader, final T parentBuilder) {
		super(clauseHeader, parentBuilder);
		selectionFieldsBuilder = SelectionFieldBuilderFactory.createSelectionFieldBuilder(this);
	}

}
