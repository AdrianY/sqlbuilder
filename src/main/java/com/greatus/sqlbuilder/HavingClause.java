package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsCountAggregate;
import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsCountDistinctAggregate;
import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsMaxAggregate;
import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsMinAggregate;
import static com.greatus.sqlbuilder.SQLBuilderAggergateUtil.wrapAsSumAggregate;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_AND;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_NONE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_OR;

import com.greatus.sqlbuilder.ConditionFieldFactory.HavingField;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilder;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilderFactory;

public final class HavingClause {
	private HavingClause(){}
	
	private static final String HAVING_CLAUSE_HEADER = "HAVING";
	
	static final class HavingClauseBuilderFactory {
		private HavingClauseBuilderFactory(){}
		
		static HavingClauseBuilder generateHavingClauseBuilder(final ActualSelectBuilder parentBuilder){
			return new HavingClauseBuilder(parentBuilder);
		}
	}
	
	public static class HavingClauseBuilder extends AbstractSQLClauseBuilder<ActualSelectBuilder>{
		private HavingClauseBuilder(final ActualSelectBuilder parentBuilder){
			super(HAVING_CLAUSE_HEADER, parentBuilder);
		}
		
		private HavingField noLogicialOperatorField(final String fieldName){						
			return ConditionFieldFactory.generateHavingField(LOGICAL_OPER_NONE, fieldName, this);
		}
		
		private HavingField andLogicalOperatorField(final String fieldName){						
			return ConditionFieldFactory.generateHavingField(LOGICAL_OPER_AND, fieldName, this);
		}
		
		private HavingField orLogicalOperatorField(final String fieldName){						
			return ConditionFieldFactory.generateHavingField(LOGICAL_OPER_OR, fieldName, this);
		}
		
		public HavingField countField(String fieldName){
			return noLogicialOperatorField(wrapAsCountAggregate(fieldName));
		}
		
		public HavingField sumField(String fieldName){
			return noLogicialOperatorField(wrapAsSumAggregate(fieldName));
		}
		
		public HavingField minField(String fieldName){
			return noLogicialOperatorField(wrapAsMinAggregate(fieldName));
		}
		
		public HavingField maxField(String fieldName){
			return noLogicialOperatorField(wrapAsMaxAggregate(fieldName));
		}
		
		public HavingField countDistinctField(String fieldName){
			return noLogicialOperatorField(wrapAsCountDistinctAggregate(fieldName));
		}
		
		public HavingField andCountField(String fieldName){
			return andLogicalOperatorField(wrapAsCountAggregate(fieldName));
		}
		
		public HavingField andSumField(String fieldName){
			return andLogicalOperatorField(wrapAsSumAggregate(fieldName));
		}
		
		public HavingField andMinField(String fieldName){
			return andLogicalOperatorField(wrapAsMinAggregate(fieldName));
		}
		
		public HavingField andMaxField(String fieldName){
			return andLogicalOperatorField(wrapAsMaxAggregate(fieldName));
		}
		
		public HavingField andCountDistinctField(String fieldName){
			return andLogicalOperatorField(wrapAsCountDistinctAggregate(fieldName));
		}
		
		public HavingField orCountField(String fieldName){
			return orLogicalOperatorField(wrapAsCountAggregate(fieldName));
		}
		
		public HavingField orSumField(String fieldName){
			return orLogicalOperatorField(wrapAsSumAggregate(fieldName));
		}
		
		public HavingField orMinField(String fieldName){
			return orLogicalOperatorField(wrapAsMinAggregate(fieldName));
		}
		
		public HavingField orMaxField(String fieldName){
			return orLogicalOperatorField(wrapAsMaxAggregate(fieldName));
		}
		
		public HavingField orCountDistinctField(String fieldName){
			return orLogicalOperatorField(wrapAsCountDistinctAggregate(fieldName));
		}
		
		public OrderByClauseBuilder orderBy(){
			parentBuilder.setOrderByClause(OrderByClauseBuilderFactory.generateOrderByClauseBuilder(parentBuilder));
			return parentBuilder.getOrderByClause();
		}
	}
}
