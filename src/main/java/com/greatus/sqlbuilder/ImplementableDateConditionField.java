package com.greatus.sqlbuilder;

interface ImplementableDateConditionField<T/* extends AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>*/, P> {
	T isOn(P value, String format);
	
	T isNotOn(P value, String format);
	
	T isAfter(P value, String format);
	
	T isOnOrAfter(P value, String format);
	
	T before(P value, String format);
	
	T onOrBefore(P value, String format);
	
	T in(P[] value, String format);
	
	T notIn(P[] value, String format);
	
	T between(P lowerBoundValue, P upperBoundValue, String format);
	
	T notBetween(P lowerBoundValue, P upperBoundValue, String format);
}
