package com.greatus.lazysqlbuilder;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.Map;

public final class SQLBuilderStrUtil {
	private SQLBuilderStrUtil(){}
	
	
	private static Map<String, String> dateToSQLFormatCache;
	
	static{
		dateToSQLFormatCache = new HashMap<String , String>();
		dateToSQLFormatCache.put("MM/dd/YYYY", "MM/DD/YYYY");
	}
	
	public static final String SPACE = " ";
	public static final String NEW_LINE = "\n";
	public static final String TAB = "\t";
	public static final String COMMA = ",";
	public static final String PERCENTAGE = "%";
	public static final String OPEN_PARENTHESIS = "(";
	public static final String CLOSE_PARENTHESIS = ")";
	public static final String SINGLE_QUOTE = "'";
	public static final String EMPTY_STR = "";
	public static final String OPEN_CURLY_BRACE = "{";
	public static final String CLOSE_CURLY_BRACE = "}";
	
	public static final String LOGICAL_OPER_AND = "AND";
	public static final String LOGICAL_OPER_OR = "OR";
	public static final String LOGICAL_OPER_NONE = "";
	
	public static String wrapWithPrependedSpace(String value){
		return !isEmpty(value) ? SPACE+value : EMPTY_STR;
	}
	
	public static String wrapAsValue(String value){
		return SINGLE_QUOTE+(!isEmpty(value) ? value : EMPTY_STR)+SINGLE_QUOTE;
	}
	
	public static String wrapWithParenthesis(String value){
		return OPEN_PARENTHESIS+(!isEmpty(value) ? value : EMPTY_STR)+CLOSE_PARENTHESIS;
	}
	
	public static String wrapWithCurlyBraces(String value){
		return OPEN_CURLY_BRACE+(!isEmpty(value) ? value : EMPTY_STR)+CLOSE_CURLY_BRACE;
	}
	
	public static String wrapAsToCharField(String fieldname, String format){
		return "TO_CHAR"+wrapWithParenthesis(fieldname+COMMA+wrapWithPrependedSpace(wrapAsValue(getEquivalentSQLDateFormat(format))));
	}
	
	public static boolean isEmpty(final String value){
		return null == value || EMPTY_STR.equals(value);
	}
	
	public static String formatDate(final LocalDate localDate, final String format){
		return null != localDate ? localDate.format(DateTimeFormatter.ofPattern(format)).toString() : EMPTY_STR;
	}
	
	public static String getEquivalentSQLDateFormat(final String localDateFormat){
		return dateToSQLFormatCache.get(localDateFormat);
	}

}
