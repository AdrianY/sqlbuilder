package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.COMMA;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.SPACE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.EMPTY_STR;

final class SelectionFieldBuilderFactory {
	private SelectionFieldBuilderFactory(){}
	
	public static  <T extends AbstractHeaderSQLClauseBuilder> SelectionFieldBuilder createSelectionFieldBuilder(final AbstractSQLClauseBuilder<T> parentBuilder){
		return new SelectionFieldBuilder(parentBuilder);
	}
	
	static class SelectionFieldBuilder{
		private AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder> parentBuilder;
		private Finalizer appendFinalizer;
		
		private <T extends AbstractHeaderSQLClauseBuilder> SelectionFieldBuilder(final AbstractSQLClauseBuilder<T> parentBuilder){
			this.parentBuilder = parentBuilder;
			appendFinalizer = new Finalizer();
		}
		
		void doAppendRawStringField(final String fieldName){
			parentBuilder.getStrBuilder().append(
					appendFinalizer.finalizeToBeAppendedValue(fieldName));
		}
	}
	
	private static class Finalizer{
		private boolean appendComma;
		
		private Finalizer(){
			appendComma = false;
		}
		
		private String finalizeToBeAppendedValue(final String rawString){
			String toBeAppended = EMPTY_STR;
			if(appendComma)
				toBeAppended = (COMMA);
			toBeAppended += SPACE;
			toBeAppended += rawString;
			
			if(!appendComma)
				appendComma = true;
			
			return toBeAppended;
		}
	}
}
