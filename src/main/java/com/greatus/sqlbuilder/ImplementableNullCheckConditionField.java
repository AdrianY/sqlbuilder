package com.greatus.sqlbuilder;

interface ImplementableNullCheckConditionField<T> {
	T isNull();
	
	T isNotNull();
}
