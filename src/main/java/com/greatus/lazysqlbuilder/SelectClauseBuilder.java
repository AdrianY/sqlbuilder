package com.greatus.lazysqlbuilder;


class SelectClauseBuilder extends AbstractHeadSQLClauseBuilder {
	
	public static SelectClauseBuilder createInstance(){
		return new SelectClauseBuilder(HeadSQLType.SELECT);
	}

	private SelectClauseBuilder(final HeadSQLType sqlClause) {
		super(sqlClause);
	}

}
