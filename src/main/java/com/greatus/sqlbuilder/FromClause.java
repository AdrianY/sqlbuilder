package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapWithPrependedSpace;

import com.greatus.sqlbuilder.GroupByClause.GroupByClauseBuilder;
import com.greatus.sqlbuilder.GroupByClause.GroupByClauseBuilderFactory;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilder;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilderFactory;
import com.greatus.sqlbuilder.WhereClause.AbstractWhereClauseBuilder;
import com.greatus.sqlbuilder.WhereClause.WhereClauseBuilderFactory;
import com.greatus.sqlbuilder.WhereClause.WhereClauseForSQLSelectBuilder;

public final class FromClause {
	private FromClause(){}
	
	private static final String FROM_CLAUSE_HEADER = "FROM";
	
	static final class FromClauseBuilderFactory {
		
		private FromClauseBuilderFactory(){}
		
		static FromClauseForSelectSQLBuilder generateFromClauseForSelectSQLBuilder(final ActualSelectBuilder parentBuilder){
			return new FromClauseForSelectSQLBuilder(parentBuilder);
		}
	}
	
	static private interface ImplementableFromClause<P, E extends AbstractWhereClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>>{
		P table(String tableName);
		
		E where();
	}
	
	static abstract class AbstractFromClauseBuilder<
		P extends AbstractFromClauseBuilder<?,? extends AbstractHeaderSQLClauseBuilder>,
		T extends AbstractHeaderSQLClauseBuilder
	> extends AbstractSQLClauseBuilder<T>{
		
		private AbstractFromClauseBuilder(final T parentBuilder){
			super(FROM_CLAUSE_HEADER, parentBuilder);
		}
		
		public abstract P table(String tableName);
	}
	
	public static class FromClauseForSelectSQLBuilder extends AbstractFromClauseBuilder<FromClauseForSelectSQLBuilder,ActualSelectBuilder>
		implements ImplementableFromClause<FromClauseForSelectSQLBuilder, WhereClauseForSQLSelectBuilder>{
		
		private FromClauseForSelectSQLBuilder(final ActualSelectBuilder parentBuilder){
			super(parentBuilder);
		}
		
		public FromClauseForSelectSQLBuilder table(String tableName){
			clauseBuilder.append(wrapWithPrependedSpace(tableName));
			return this;
		}
		
		public WhereClauseForSQLSelectBuilder where(){
			parentBuilder.setWhereClause(WhereClauseBuilderFactory.generateWhereClauseForSQLSelectBuilder(parentBuilder));
			return parentBuilder.getWhereClause();
		} 
		
		public GroupByClauseBuilder groupBy(){
			parentBuilder.setGroupByClause(GroupByClauseBuilderFactory.generateGroupByClauseBuilder(parentBuilder));
			return parentBuilder.getGroupByClause();
		}
		
		public OrderByClauseBuilder orderBy(){
			parentBuilder.setOrderByClause(OrderByClauseBuilderFactory.generateOrderByClauseBuilder(parentBuilder));
			return parentBuilder.getOrderByClause();
		}
	}
}
