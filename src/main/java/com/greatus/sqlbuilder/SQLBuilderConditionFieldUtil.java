package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.COMMA;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.EMPTY_STR;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_AND;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_NONE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.SPACE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.NEW_LINE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.TAB;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.PERCENTAGE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.SINGLE_QUOTE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapWithCurlyBraces;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.isEmpty;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapWithPrependedSpace;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapAsValue;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

final class SQLBuilderConditionFieldUtil {
	private SQLBuilderConditionFieldUtil(){}
	
	public static final String OPER_EQUALS = "=";
	public static final String OPER_NOT_EQUALS = "<>";
	public static final String OPER_GREATER_THAN = ">";
	public static final String OPER_GREATER_THAN_OR_EQ = ">=";
	public static final String OPER_LESS_THAN = "<";
	public static final String OPER_LESS_THAN_OR_EQ = "<=";
	public static final String OPER_IN = "IN";
	public static final String OPER_NOT_IN = "NOT IN";
	public static final String OPER_BETWEEN = "BETWEEN";
	public static final String OPER_NOT_BETWEEN = "NOT BETWEEN";
	public static final String OPER_LIKE = "LIKE";
	public static final String OPER_NOT_LIKE = "NOT LIKE";
	public static final String IS_NULL = "IS NULL";
	public static final String IS_NOT_NULL = "IS NOT NULL";
	
	static String appendEqualsClause(final String logicalOperator, final String finalizedField, final String value){
		return appendWhereClauseComponent(logicalOperator, OPER_EQUALS, finalizedField, value);
	}
	
	static String appendNotEqualsClause(final String logicalOperator, final String finalizedField, final String value){
		return appendWhereClauseComponent(logicalOperator, OPER_NOT_EQUALS, finalizedField, value);
	}
	
	static String appendGreaterThanClause(final String logicalOperator, final String finalizedField, final String value){
		return appendWhereClauseComponent(logicalOperator, OPER_GREATER_THAN, finalizedField, value);
	}
	
	static String appendGreaterThanOrEqualsClause(final String logicalOperator, final String finalizedField, final String value){
		return appendWhereClauseComponent(logicalOperator, OPER_GREATER_THAN_OR_EQ, finalizedField, value);
	}
	
	static String appendLessThanClause(final String logicalOperator, final String finalizedField, final String value){
		return appendWhereClauseComponent(logicalOperator, OPER_LESS_THAN, finalizedField, value);
	}
	
	static String appendLessThanOrEqualsClause(final String logicalOperator, final String finalizedField, final String value){
		return appendWhereClauseComponent(logicalOperator, OPER_LESS_THAN_OR_EQ, finalizedField, value);
	}
	
	static String appendInClause(final String logicalOperator, final String finalizedField, final String[] values, final boolean areNumeric){
		String sqlOperationValues = finalizeInOperValues(values, areNumeric);
		return appendWhereClauseComponent(logicalOperator, OPER_IN, finalizedField, sqlOperationValues);
	}
	
	static String appendNotInClause(final String logicalOperator, final String finalizedField, final String[] values, final boolean areNumeric){
		String sqlOperationValues = finalizeInOperValues(values, areNumeric);
		return appendWhereClauseComponent(logicalOperator, OPER_NOT_IN, finalizedField, sqlOperationValues);
	}
	
	static String appendBetweenClause(final String logicalOperator, final String finalizedField, final String lowerBoundValue, final String upperBoundValue){
		String sqlOperationValue = finalizeBetweenOperValues(lowerBoundValue, upperBoundValue);
		return appendWhereClauseComponent(logicalOperator, OPER_BETWEEN, finalizedField, sqlOperationValue);
	}
	
	static String appendNotBetweenClause(final String logicalOperator, final String finalizedField, final String lowerBoundValue, final String upperBoundValue){
		String sqlOperationValue = finalizeBetweenOperValues(lowerBoundValue, upperBoundValue);
		return appendWhereClauseComponent(logicalOperator, OPER_NOT_BETWEEN, finalizedField, sqlOperationValue);
	}
	
	static String appendLikeClause(final String logicalOperator, final String finalizedField, final String value){
		String sqlOperationValues = finalizeLikeValues(value);
		return appendWhereClauseComponent(logicalOperator, OPER_LIKE, finalizedField, sqlOperationValues);
	}
	
	static String appendNotLikeClause(final String logicalOperator, final String finalizedField, final String value){
		String sqlOperationValues = finalizeLikeValues(value);
		return appendWhereClauseComponent(logicalOperator, OPER_NOT_LIKE, finalizedField, sqlOperationValues);
	}
	
	static String appendIsNullClause(final String logicalOperator, final String finalizedField){
		return appendWhereClauseNullCheckComponent(logicalOperator, IS_NULL, finalizedField);
	}
	
	static String appendIsNotNullClause(final String logicalOperator, final String finalizedField){
		return appendWhereClauseNullCheckComponent(logicalOperator, IS_NOT_NULL, finalizedField);
	}
	
	private static String finalizeInOperValues(final String [] values, final boolean areNumeric){
		String sqlOperationValue = EMPTY_STR;
		
		if(null != values && 0 < values.length){
			List<String> valuesAsList = new LinkedList<String>(Arrays.asList(values));
			sqlOperationValue = finalizedInOperValue(valuesAsList.remove(0), areNumeric);
			for(String inListValue: valuesAsList){
				sqlOperationValue += COMMA;
				sqlOperationValue += finalizedInOperValue(inListValue, areNumeric);
			}
		}
		
		return wrapWithCurlyBraces(sqlOperationValue);
	}
	
	private static String finalizedInOperValue(final String value, final boolean areNumeric){
		return areNumeric ? value : wrapAsValue(value);
	}
	
	private static String finalizeLikeValues(final String value){
		return !isEmpty(value) 
			   ? SINGLE_QUOTE + PERCENTAGE + value + PERCENTAGE + SINGLE_QUOTE
			   : SPACE;
	}
	
	private static String finalizeBetweenOperValues(final String lowerBoundValue, final String upperBoundValue){
		return !isEmpty(lowerBoundValue) && !isEmpty(upperBoundValue) 
			   ? lowerBoundValue + SPACE + LOGICAL_OPER_AND + wrapWithPrependedSpace(upperBoundValue) 
			   : SPACE;
	}
	
	private static String appendWhereClauseComponent( 
			final String logicalOperator,
			final String operator,
			final String finalizedField, 
			final String value
	){
		String builtString = EMPTY_STR;
		if(!isEmpty(value)){
			builtString += logicalOperator;
			builtString += wrapWithPrependedSpace(finalizedField);
			builtString += wrapWithPrependedSpace(operator);
			builtString += wrapWithPrependedSpace(value);
		}
		return builtString;
	}
	
	private static String appendWhereClauseNullCheckComponent(
			final String logicalOperator,
			final String operator,
			final String finalizedField
	){
		String builtString = wrapWithPrependedSpace(logicalOperator);
		builtString += wrapWithPrependedSpace(finalizedField);
		builtString += wrapWithPrependedSpace(operator);
		return builtString;
	}
}
