package com.greatus.sqlbuilder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	SelectBuilderTest.class,
	SelectBuilderWhereClauseTest.class,
	SelectBuilderWhereClauseWithMultipleConditionClauseTest.class
})
public class TestSuite {

}
