package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.*;

import com.greatus.sqlbuilder.FromClause.FromClauseForSelectSQLBuilder;
import com.greatus.sqlbuilder.GroupByClause.GroupByClauseBuilder;
import com.greatus.sqlbuilder.HavingClause.HavingClauseBuilder;
import com.greatus.sqlbuilder.OrderByClause.OrderByClauseBuilder;
import com.greatus.sqlbuilder.SelectClause.SelectClauseBuilder;
import com.greatus.sqlbuilder.SelectClause.SelectClauseBuilderFactory;
import com.greatus.sqlbuilder.WhereClause.WhereClauseForSQLSelectBuilder;


public class ActualSelectBuilder extends AbstractHeaderSQLClauseBuilder 
	implements HasFromClauseSQLBuilder<FromClauseForSelectSQLBuilder>, 
			   HasWhereClauseSQLBuilder<WhereClauseForSQLSelectBuilder>,
			   HasGroupByClauseSQLBuilder,
			   HasHavingClauseSQLBuilder,
			   HasOrderByClauseSQLBuilder{
	
	private FromClauseForSelectSQLBuilder fromClause;
	private SelectClauseBuilder selectClause;
	private WhereClauseForSQLSelectBuilder whereClause;
	private GroupByClauseBuilder groupByClause;
	private OrderByClauseBuilder orderByClause;
	private HavingClauseBuilder havingClause;
	
	ActualSelectBuilder(){
		super(HEAD_SQL_TYPE_SELECT);
	}
	
	SelectClauseBuilder getSelectClause(){
		return selectClause;
	}
	
	public SelectClauseBuilder select(){
		if(null == selectClause)			
			selectClause = SelectClauseBuilderFactory.generateSelectClauseBuilder(this);
		
		return selectClause;
	}
	
	@Override
	public FromClauseForSelectSQLBuilder getFromClause(){	
		return fromClause;
	}
	
	@Override
	public void setFromClause(final FromClauseForSelectSQLBuilder fromClause){
		if(null == this.fromClause)
			this.fromClause = fromClause;
	}	

	@Override
	public WhereClauseForSQLSelectBuilder getWhereClause() {
		return whereClause;
	}

	@Override
	public void setWhereClause(WhereClauseForSQLSelectBuilder whereClause) {
		if(null == this.whereClause)
			this.whereClause = whereClause;
	}

	@Override
	public GroupByClauseBuilder getGroupByClause() {
		return groupByClause;
	}

	@Override
	public void setGroupByClause(GroupByClauseBuilder groupByClause) {
		if(null == this.groupByClause)
			this.groupByClause = groupByClause;		
	}

	@Override
	public OrderByClauseBuilder getOrderByClause() {	
		return orderByClause;
	}

	@Override
	public void setOrderByClause(OrderByClauseBuilder orderByClause) {
		if(null == this.orderByClause)
			this.orderByClause = orderByClause;
	}

	@Override
	public HavingClauseBuilder getHavingClause() {
		return havingClause;
	}

	@Override
	public void setHavingClause(HavingClauseBuilder havingClause) {
		if(null == this.havingClause)
			this.havingClause = havingClause;
	}
}
