package com.greatus.sqlbuilder;

import static org.junit.Assert.assertEquals;

import java.time.LocalDate;
import java.time.Month;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import com.greatus.sqlbuilder.FromClause.FromClauseForSelectSQLBuilder;
import com.greatus.sqlbuilder.WhereClause.WhereClauseForSQLSelectBuilder;

@RunWith(Parameterized.class)
public class SelectBuilderWhereClauseWithMultipleConditionClauseTest {

	private String expectedSQL;
	private WhereClauseForSQLSelectBuilder actualSQLBuilder;
	
	public SelectBuilderWhereClauseWithMultipleConditionClauseTest(StringBuilder inputExpectedSQL, WhereClauseForSQLSelectBuilder inputActualSQLBuilder){
		expectedSQL = inputExpectedSQL.toString();
		actualSQLBuilder = inputActualSQLBuilder;
	}
	
	private static final String DATE_FORMAT = "MM/dd/YYYY";
	
	private static StringBuilder generateBaseExpectedSQLBuilder(){
		return new StringBuilder().append("SELECT *").append("\n").append("FROM sample_table");
	}
	
	private static FromClauseForSelectSQLBuilder generateBaseActualSQLBuilder(){
		SelectBuilder selectBuiler = SQLBuilderFactory.generateSelectBuilder();
		return selectBuiler
					.select().all()
					.from().table("sample_table");
	}
	
	@Parameters
	public static Iterable<Object[]> testParams(){
		Object[][] testParams = new Object[][]{
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field = 'name'").append("\n")
						.append("\t").append("AND my_numeric_field > 2").append("\n")
						.append("\t").append("OR TO_CHAR(my_date_field, 'MM/DD/YYYY') BETWEEN '01/09/1991' AND '01/09/2015'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").equalTo("name")
								.andField("my_numeric_field").greaterThan(2)
								.orField("my_date_field").between(
										LocalDate.of(1991, Month.JANUARY, 9), LocalDate.of(2015, Month.JANUARY, 9), DATE_FORMAT)
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field <> 'name'").append("\n")
						.append("\t").append("AND (").append("\n")
						.append("\t\t").append("TO_CHAR(my_date_field, 'MM/DD/YYYY') IN {'01/09/1991','02/09/1991','03/09/1991'}").append("\n")
						.append("\t\t").append("OR my_numeric BETWEEN 1000 AND 2000").append("\n")
						.append("\t").append(")").append("\n")
						.append("\t").append("OR my_field LIKE '%poupy%'"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notEqualTo("name")
								.and().field("my_date_field").in(new LocalDate[]{LocalDate.of(1991, Month.JANUARY, 9),LocalDate.of(1991, Month.FEBRUARY, 9),LocalDate.of(1991, Month.MARCH, 9)}, DATE_FORMAT)
									  .orField("my_numeric").between(1000, 2000)
									  .end()
								.orField("my_field").like("poupy")
				},
				{
					generateBaseExpectedSQLBuilder().append("\n")
						.append("WHERE my_field <> 'name'").append("\n")
						.append("\t").append("OR (").append("\n")
						.append("\t\t").append("TO_CHAR(my_date_field, 'MM/DD/YYYY') IN {'01/09/1991','02/09/1991','03/09/1991'}").append("\n")
						.append("\t\t").append("AND my_numeric BETWEEN 1000 AND 2000").append("\n")
						.append("\t").append(")").append("\n")
						.append("\t").append("AND my_field NOT LIKE '%poupy%'").append("\n")
						.append("\t").append("AND (").append("\n")
						.append("\t\t").append("TO_CHAR(my_date_field, 'MM/DD/YYYY') NOT IN {'01/09/2015','02/09/2015','03/09/2015'}").append("\n")
						.append("\t\t").append("OR my_numeric BETWEEN 1000 AND 2000").append("\n")
						.append("\t").append(")").append("\n"),
					generateBaseActualSQLBuilder()
						.where().field("my_field").notEqualTo("name")
								.or().field("my_date_field").in(new LocalDate[]{LocalDate.of(1991, Month.JANUARY, 9),LocalDate.of(1991, Month.FEBRUARY, 9),LocalDate.of(1991, Month.MARCH, 9)}, DATE_FORMAT)
									  .andField("my_numeric").between(1000, 2000)
									  .end()
								.andField("my_field").notLike("poupy")
								.and().field("my_date_field").notIn(new LocalDate[]{LocalDate.of(2015, Month.JANUARY, 9),LocalDate.of(2015, Month.FEBRUARY, 9),LocalDate.of(2015, Month.MARCH, 9)}, DATE_FORMAT)
									  .orField("my_numeric").between(1000, 2000)
									  .end()
				}
		};
		return Arrays.asList(testParams);
	}
	
	@Test
	public void constructedSelectWithMultipleConditionShouldBeEqualToExpected() throws SQLBuilderException{
		assertEquals("Result",expectedSQL,SQLBuilderExecuter.stringifySQLBuilder(actualSQLBuilder));
	}
}
