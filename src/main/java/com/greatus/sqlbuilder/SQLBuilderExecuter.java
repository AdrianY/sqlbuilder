package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.*;

public final class SQLBuilderExecuter {
	private SQLBuilderExecuter(){}
	
	public static String stringifySQLBuilder(final AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder> sqlClauseBuilder) throws SQLBuilderException{
		return stringifySQLBuilder(sqlClauseBuilder.getParentBuilder());
	}
	
	public static String stringifySQLBuilder(final AbstractSQLBuilderFacade<? extends AbstractHeaderSQLClauseBuilder> sqlBuilder) throws SQLBuilderException{
		return stringifySQLBuilder(sqlBuilder.getWrappedActualBuilderInstance());
	}
	
	private static String stringifySQLBuilder(final AbstractHeaderSQLClauseBuilder actualSqlBuilder) throws SQLBuilderException{
		String constructedSQL = actualSqlBuilder.getSqlClauseType();
		switch(constructedSQL){
			case HEAD_SQL_TYPE_SELECT:
				constructedSQL = stringifySQLSelect((ActualSelectBuilder)actualSqlBuilder);
		}
		return constructedSQL;
	}
	
	private static String stringifySQLSelect(final ActualSelectBuilder selectBuilder) throws SQLBuilderException{
		StringBuilder constructedSQL = selectBuilder.getSelectClause().getStrBuilder();
		appendIfNotEmpty(selectBuilder.getFromClause(), constructedSQL);
		appendIfNotEmpty(selectBuilder.getWhereClause(), constructedSQL);
		appendIfNotEmpty(selectBuilder.getGroupByClause(), constructedSQL);
		appendIfNotEmpty(selectBuilder.getHavingClause(), constructedSQL);
		appendIfNotEmpty(selectBuilder.getOrderByClause(), constructedSQL);
		return constructedSQL.toString();
	}
	
	private static void appendIfNotEmpty(
			final AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder> sqlClauseBuilder, 
			final StringBuilder constructedSQL
	){
		if(null != sqlClauseBuilder){
			constructedSQL.append(NEW_LINE).append(sqlClauseBuilder.getStrBuilder().toString());
		}
	}
}
