package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.EMPTY_STR;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.LOGICAL_OPER_NONE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.NEW_LINE;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.TAB;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapAsToCharField;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapAsValue;
import static com.greatus.sqlbuilder.SQLBuilderStrUtil.formatDate;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.greatus.sqlbuilder.HavingClause.HavingClauseBuilder;
import com.greatus.sqlbuilder.WhereClause.WhereClauseForSQLSelectBuilder;

final class ConditionFieldFactory {
	private ConditionFieldFactory(){}
	
	static WhereFieldForSQLSelect generateWhereFieldForSelectSQL(String logicalOperator, String fieldName,
			WhereClauseForSQLSelectBuilder parentBuilder){
		return new WhereFieldForSQLSelect(logicalOperator, fieldName,parentBuilder);
	}
	
	static HavingField generateHavingField(String logicalOperator, String fieldName,
			HavingClauseBuilder parentBuilder){
		return new HavingField(logicalOperator, fieldName,parentBuilder);
	}
	
	public static class WhereFieldForSQLSelect extends ConditionField<WhereClauseForSQLSelectBuilder>{

		private WhereFieldForSQLSelect(
				final String logicalOperator, final String fieldName, final WhereClauseForSQLSelectBuilder parentWhereBuilder) {
			super(logicalOperator, fieldName, parentWhereBuilder);
		}
		
	}
	
	public static class HavingField extends ConditionField<HavingClauseBuilder>{

		private HavingField(
				final String logicalOperator, final String fieldName, final HavingClauseBuilder parentWhereBuilder) {
			super(logicalOperator, fieldName, parentWhereBuilder);
		}
		
	}
		
	
	static abstract class ConditionField<P extends AbstractSQLClauseBuilder<? extends AbstractHeaderSQLClauseBuilder>> 
		implements ImplementableStringConditionField<P>, 
				   ImplementableNumberConditionField<P>, 
				   ImplementableDateConditionField<P, LocalDate>,
				   ImplementableNullCheckConditionField<P>{
		
		private String fieldName;
		private P parentConditionalClauseBuilder;
		private String logicalOperator;
		
		private ConditionField(final String logicalOperator, final String fieldName, final P parentWhereBuilder){
			this.logicalOperator = logicalOperator;
			this.fieldName = fieldName;
			this.parentConditionalClauseBuilder = parentWhereBuilder;
		}
		
		@Override
		public P equalTo(String value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendEqualsClause(logicalOperator,fieldName,wrapAsValue(value)));
		}
		
		public P notEqualTo(final String value){
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotEqualsClause(logicalOperator,fieldName,wrapAsValue(value)));
		}
		
		@Override
		public P in(String[] value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendInClause(logicalOperator,fieldName,value,false));
		}
	
		@Override
		public P notIn(String[] value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotInClause(logicalOperator,fieldName,value,false));
		}
		
		@Override
		public P like(String value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLikeClause(logicalOperator,fieldName,value));
		}
	
		@Override
		public P notLike(String value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotLikeClause(logicalOperator,fieldName,value));
		}
	
		@Override
		public P isOn(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendEqualsClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P isNotOn(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotEqualsClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P isAfter(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendGreaterThanClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P isOnOrAfter(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendGreaterThanOrEqualsClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P before(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLessThanClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P onOrBefore(LocalDate value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLessThanOrEqualsClause(
							logicalOperator,wrapAsToCharField(fieldName, format), wrapAsValue(formatDate(value, format))));
		}
	
		@Override
		public P in(LocalDate[] value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendInClause(
							logicalOperator,wrapAsToCharField(fieldName, format), 
							stringifyArray(finalizetoBeStringifiedArray(value, format)), false));
		}
	
		@Override
		public P notIn(LocalDate[] value, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotInClause(
							logicalOperator,wrapAsToCharField(fieldName, format), 
							stringifyArray(finalizetoBeStringifiedArray(value, format)), false));
		}
	
		@Override
		public P between(LocalDate lowerBoundValue,
				LocalDate upperBoundValue, String format) {
			return appendToParentBuilder(
						SQLBuilderConditionFieldUtil.appendBetweenClause(
								logicalOperator,
								wrapAsToCharField(fieldName, format), 
								wrapAsValue(formatDate(lowerBoundValue, format)), 
								wrapAsValue(formatDate(upperBoundValue, format))
						)
					);
		}
	
		@Override
		public P notBetween(LocalDate lowerBoundValue,
				LocalDate upperBoundValue, String format) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotBetweenClause(
							logicalOperator,
							wrapAsToCharField(fieldName, format), 
							wrapAsValue(formatDate(lowerBoundValue, format)), 
							wrapAsValue(formatDate(upperBoundValue, format))
					)
				);
		}
	
		@Override
		public P equalTo(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendEqualsClause(
							logicalOperator, fieldName,value.toString()));
		}
	
		@Override
		public P notEqualTo(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotEqualsClause(
							logicalOperator, fieldName,value.toString()));
		}
	
		@Override
		public P greaterThan(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendGreaterThanClause(logicalOperator,fieldName,value.toString()));
		}
	
		@Override
		public P greaterThanOrEqual(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendGreaterThanOrEqualsClause(logicalOperator,fieldName,value.toString()));
		}
	
		@Override
		public P lessThan(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLessThanClause(
							logicalOperator, fieldName,value.toString()));
		}
	
		@Override
		public P lessThanOrEqual(Number value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendLessThanOrEqualsClause(logicalOperator,fieldName,value.toString()));
		}
	
		@Override
		public P in(Number[] value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendInClause(
							logicalOperator, fieldName,stringifyArray(value), true));
		}
	
		@Override
		public P notIn(Number[] value) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotInClause(
							logicalOperator, fieldName,stringifyArray(value), true));
		}
	
		@Override
		public P between(Number lowerBoundValue,
				Number upperBoundValue) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendBetweenClause(
							logicalOperator, fieldName,lowerBoundValue.toString(), upperBoundValue.toString()));
		}
	
		@Override
		public P notBetween(Number lowerBoundValue,
				Number upperBoundValue) {
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendNotBetweenClause(
							logicalOperator, fieldName,lowerBoundValue.toString(), upperBoundValue.toString()));
		}
		
		public P isNull(){
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendIsNullClause(logicalOperator, fieldName));
		}
		
		public P isNotNull(){
			return appendToParentBuilder(
					SQLBuilderConditionFieldUtil.appendIsNotNullClause(logicalOperator, fieldName));
		}
		
		private <T> String[] stringifyArray(final T[] arrayOfNumbers){
			String[] stringifiedNumberArray = {};
			if(null != arrayOfNumbers && 0 < arrayOfNumbers.length){
				Arrays.sort(arrayOfNumbers);
				stringifiedNumberArray = Arrays.toString(arrayOfNumbers).split("[\\[\\]]")[1].split(", "); 
			}
			
			return stringifiedNumberArray;
		}
		
		private static <T>  String[] finalizetoBeStringifiedArray(final T[] arrayToBeStringified, final String format){
			List<String> returnValue = new ArrayList<String>();
			if(null != arrayToBeStringified && LocalDate.class.equals(arrayToBeStringified.getClass().getComponentType())){
				for(T elementtoBeStringified: arrayToBeStringified){
					returnValue.add(formatDate(LocalDate.class.cast(elementtoBeStringified), format));
				}
			} 			
			
			return (String[]) returnValue.toArray();
		}
		
		
		
		private P appendToParentBuilder(final String conditionClause){
			String builtString = !LOGICAL_OPER_NONE.equals(logicalOperator) 
								 ? NEW_LINE + appendableTabs(parentConditionalClauseBuilder.getParentBuilder().getHierarchyNo()) 
								 : EMPTY_STR ;
			parentConditionalClauseBuilder.getStrBuilder().append(builtString).append(conditionClause);
			return parentConditionalClauseBuilder;
		}
		
		private static String appendableTabs(final int requiredNoOfTabs){
			String appendableTabs = TAB;
			for(int i = 0; i < requiredNoOfTabs; i++){
				appendableTabs += TAB;
			}
			return appendableTabs;
		}
	}
}
