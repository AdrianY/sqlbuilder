package com.greatus.sqlbuilder;

public abstract class AbstractSQLClauseBuilder<T extends AbstractHeaderSQLClauseBuilder> {
	protected T parentBuilder;
	protected StringBuilder clauseBuilder;
	
	AbstractSQLClauseBuilder(final String clauseHeader, final T parentBuilder){
		this.parentBuilder = parentBuilder;
		clauseBuilder = new StringBuilder(clauseHeader);	
	}
	
	StringBuilder getStrBuilder(){
		return clauseBuilder;
	}
	
	T getParentBuilder(){
		return parentBuilder;
	}
}
