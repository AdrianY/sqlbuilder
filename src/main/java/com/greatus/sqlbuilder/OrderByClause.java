package com.greatus.sqlbuilder;

import static com.greatus.sqlbuilder.SQLBuilderStrUtil.wrapWithPrependedSpace;

public final class OrderByClause {
	private OrderByClause(){}
	
	private static final String ORDERBY_DIR_ASC = "ASC";
	private static final String ORDERBY_DIR_DESC = "DESC";
	private static final String ORDERBY_CLAUSE_HEADER = "ORDER BY";
	
	static final class OrderByClauseBuilderFactory{
		
		private OrderByClauseBuilderFactory(){}
		
		static OrderByClauseBuilder generateOrderByClauseBuilder(final ActualSelectBuilder parentBuilder){
			return new OrderByClauseBuilder(parentBuilder);
		}
		
	}
	
	public static class OrderByClauseBuilder extends AbstractSQLSelectionClauseBuilder<ActualSelectBuilder>{
		private OrderByClauseBuilder(final ActualSelectBuilder parentBuilder){
			super(ORDERBY_CLAUSE_HEADER, parentBuilder);
		}
		
		public OrderByClauseBuilder fieldInAscendingOrder(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(fieldName+wrapWithPrependedSpace(ORDERBY_DIR_ASC));		
			return this;
		} 
		
		public OrderByClauseBuilder fieldInDescendingOrder(String fieldName){
			selectionFieldsBuilder.doAppendRawStringField(fieldName+wrapWithPrependedSpace(ORDERBY_DIR_DESC));		
			return this;
		}
	}
}
