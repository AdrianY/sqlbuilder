package com.greatus.sqlbuilder;


interface ImplementableStringConditionField<T> {
	T equalTo(final String value);
	
	T notEqualTo(final String value);
	
	T like(final String value);
	
	T notLike(final String value);
	
	T in(final String[] value);
	
	T notIn(final String[] value);
}
