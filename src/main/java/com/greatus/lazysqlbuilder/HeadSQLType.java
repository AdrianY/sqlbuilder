package com.greatus.lazysqlbuilder;

public enum HeadSQLType {
	SELECT("SELECT"),
	INSERT("INSERT"),
	UPDATE("UPDATE"),
	DELETE("DELETE");
	
	private String id;
	
	private HeadSQLType(String identifier){
		id = identifier;
	}
	
	@Override
	public String toString(){
		return id;
	}
}
