package com.greatus.sqlbuilder;

abstract class AbstractSQLBuilderFacade<T extends AbstractHeaderSQLClauseBuilder> {
	private T actualBuilderInstance;
	
	AbstractSQLBuilderFacade(final T actualBuilderInstance){
		this.actualBuilderInstance = actualBuilderInstance;
	}
	
	T getWrappedActualBuilderInstance(){
		return actualBuilderInstance;
	}
}
