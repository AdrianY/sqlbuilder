package com.greatus.sqlbuilder;

import com.greatus.sqlbuilder.FromClause.AbstractFromClauseBuilder;

interface HasFromClauseSQLBuilder<T extends AbstractFromClauseBuilder<?,? extends AbstractHeaderSQLClauseBuilder>>{
	
	T getFromClause();
	
	void setFromClause(final T fromClause);
	
}
