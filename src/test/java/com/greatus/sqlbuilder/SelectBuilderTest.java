package com.greatus.sqlbuilder;

import static org.junit.Assert.*;

import org.junit.Test;

public class SelectBuilderTest {
	
	@Test
	public void shouldBeAbleToBuildSelectAllWithFromClause() throws SQLBuilderException{
		String expectedValue = 
				new StringBuilder("SELECT *")
					.append("\n")
					.append("FROM sample_table")
					.toString();
		
		SelectBuilder selectBuiler = SQLBuilderFactory.generateSelectBuilder();
		selectBuiler.select().all().from().table("sample_table");	
		assertEquals(expectedValue, SQLBuilderExecuter.stringifySQLBuilder(selectBuiler));
	}
	
	@Test
	public void shouldBeAbleToBuildSelectGroupingWithFromWhereAndGroupClause() throws SQLBuilderException{
		String expectedValue = 
				new StringBuilder("SELECT my_group, COUNT(DISTINCT my_name)")
					.append("\n")
					.append("FROM sample_table")
					.append("\n").append("WHERE department = 'Computer Science'")
					.append("\n\t").append("AND age > 16")
					.append("\n").append("GROUP BY my_group")
					.toString();
		
		SelectBuilder selectBuiler = SQLBuilderFactory.generateSelectBuilder();
		selectBuiler.select()
						.field("my_group")
						.countDistinctField("my_name")
					.from().table("sample_table")
					.where()
						.field("department").equalTo("Computer Science")
						.andField("age").greaterThan(16)
					.groupBy()
						.field("my_group");	
		
		assertEquals(expectedValue, SQLBuilderExecuter.stringifySQLBuilder(selectBuiler));
	}
	
	@Test
	public void shouldBeAbleToBuildSelectWithHavingClause() throws SQLBuilderException{
		String expectedValue = 
				new StringBuilder("SELECT my_name, COUNT(DISTINCT my_clients), MAX(our_transactions)")
					.append("\n")
					.append("FROM sample_table_aggregated")
					.append("\n").append("WHERE my_department = 'Computer Science'")
					.append("\n\t").append("AND my_age > 16")
					.append("\n").append("GROUP BY my_clients, our_transactions")
					.append("\n").append("HAVING MAX(our_transactions) <= 99")
					.append("\n\t").append("AND COUNT(DISTINCT my_clients) >= 10")
					.toString();
		
		SelectBuilder selectBuiler = SQLBuilderFactory.generateSelectBuilder();
		selectBuiler.select()
						.field("my_name")
						.countDistinctField("my_clients")
						.maxField("our_transactions")
					.from().table("sample_table_aggregated")
					.where()
						.field("my_department").equalTo("Computer Science")
						.andField("my_age").greaterThan(16)
					.groupBy()
						.field("my_clients")
						.field("our_transactions")
					.having()
						.maxField("our_transactions").lessThanOrEqual(99)
						.andCountDistinctField("my_clients").greaterThanOrEqual(10);	
				
		assertEquals(expectedValue, SQLBuilderExecuter.stringifySQLBuilder(selectBuiler));
	}
	
	@Test
	public void shouldBeAbleToBuildSelectWithHavingAndOrderClause() throws SQLBuilderException{
		String expectedValue = 
				new StringBuilder("SELECT my_name, COUNT(DISTINCT my_clients), MAX(our_transactions)")
					.append("\n")
					.append("FROM sample_table_aggregated")
					.append("\n").append("WHERE my_department = 'Computer Science'")
					.append("\n\t").append("AND my_age > 16")
					.append("\n").append("GROUP BY my_clients, our_transactions")
					.append("\n").append("HAVING MAX(our_transactions) <= 99")
					.append("\n\t").append("AND COUNT(DISTINCT my_clients) >= 10")
					.append("\n").append("ORDER BY my_name ASC, my_age DESC, my_clients ASC")
					.toString();
		
		SelectBuilder selectBuiler = SQLBuilderFactory.generateSelectBuilder();
		selectBuiler.select()
						.field("my_name")
						.countDistinctField("my_clients")
						.maxField("our_transactions")
					.from().table("sample_table_aggregated")
					.where()
						.field("my_department").equalTo("Computer Science")
						.andField("my_age").greaterThan(16)
					.groupBy()
						.field("my_clients")
						.field("our_transactions")
					.having()
						.maxField("our_transactions").lessThanOrEqual(99)
						.andCountDistinctField("my_clients").greaterThanOrEqual(10)
					.orderBy()
						.fieldInAscendingOrder("my_name")
						.fieldInDescendingOrder("my_age")
						.fieldInAscendingOrder("my_clients");	
				
		assertEquals(expectedValue, SQLBuilderExecuter.stringifySQLBuilder(selectBuiler));
	}
}
